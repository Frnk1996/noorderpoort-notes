<?php

namespace App\Services;

use App\Models\Note;

class NoteService
{
    /**
     * @param $data
     * @return Note
     */
    public function store($data): Note
    {
        return Note::create($data);
    }

    /**
     * @param Note $note
     * @param $data
     * @return Note
     */
    public function update(Note $note, $data): Note
    {
        return tap($note)->update($data);
    }

    /**
     * @param Note $note
     * @return Note
     */
    public function destroy(Note $note): Note
    {
        return tap($note)->delete();
    }
}
