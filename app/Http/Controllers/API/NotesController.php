<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;
use App\Models\Note;
use Illuminate\Http\JsonResponse;

class NotesController extends Controller
{
    /**
     * Display a listing of all notes.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Note::all());
    }

    /**
     * Store a newly created note in storage.
     *
     * @param StoreNoteRequest $request
     * @return JsonResponse
     */
    public function store(StoreNoteRequest $request): JsonResponse
    {
        return response()->json($this->noteService->store($request->all()));
    }

    /**
     * Display the specified note.
     *
     * @param Note $note
     * @return JsonResponse
     */
    public function show(Note $note): JsonResponse
    {
        return response()->json($note);
    }

    /**
     * Update the specified note in storage.
     *
     * @param UpdateNoteRequest $request
     * @param Note $note
     * @return JsonResponse
     */
    public function update(UpdateNoteRequest $request, Note $note): JsonResponse
    {
        return response()->json($this->noteService->update($note, $request->all()));
    }

    /**
     * Remove the specified note from storage.
     *
     * @param Note $note
     * @return JsonResponse
     */
    public function destroy(Note $note): JsonResponse
    {
        return response()->json($this->noteService->destroy($note));
    }
}
