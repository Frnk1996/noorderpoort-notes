<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;
use App\Models\Note;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class NotesController extends Controller
{
    /**
     * Display a listing of all notes.
     *
     * @return View
     */
    public function index(): View
    {
        $notes = Note::all();

        return view('notes.index', compact('notes'));
    }

    /**
     * Show the form for creating a new note.
     *
     * @return View
     */
    public function create(): View
    {
        return view('notes.create');
    }

    /**
     * Store a newly created note in storage.
     *
     * @param StoreNoteRequest $request
     * @return RedirectResponse
     */
    public function store(StoreNoteRequest $request): RedirectResponse
    {
        $this->noteService->store($request->all());
        return redirect()->route('notes.index');
    }

    /**
     * Display the specified note.
     *
     * @param Note $note
     * @return View
     */
    public function show(Note $note): View
    {
        return view('notes.show', compact('note'));
    }

    /**
     * Show the form for editing the specified note.
     *
     * @param Note $note
     * @return View
     */
    public function edit(Note $note): View
    {
        return view('notes.update', compact('note'));
    }

    /**
     * Update the specified note in storage.
     *
     * @param UpdateNoteRequest $request
     * @param Note $note
     * @return RedirectResponse
     */
    public function update(UpdateNoteRequest $request, Note $note): RedirectResponse
    {
        $this->noteService->update($note, $request->all());

        return redirect()->route('notes.index');
    }

    /**
     * Remove the specified note from storage.
     *
     * @param Note $note
     * @return RedirectResponse
     */
    public function destroy(Note $note): RedirectResponse
    {
        $this->noteService->destroy($note);

        return redirect()->route('notes.index');
    }

    /**
     * Navigate to your vue root
     *
     * @return View
     */
    public function vue(): View
    {
        return view('notes.notes-spa');
    }
}
