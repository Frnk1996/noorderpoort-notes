@extends('layout.base')
@section('title', 'Note details')
@section('content')
    <a href="{{ route('notes.edit', $note) }}" class="btn btn-primary mb-3">
        Edit note
    </a>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                {{ $note->title }}
            </h5>
            <h6 class="card-subtitle mb-2 text-muted">
                Created: {{ $note->created_at }}
            </h6>

            <div class="card-text">
                {{ $note->body }}
            </div>
        </div>
    </div>
@endsection

