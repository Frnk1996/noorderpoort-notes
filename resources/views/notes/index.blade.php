@extends('layout.base')
@section('title', 'Notes overview')
@section('content')
    <a href="{{ route('notes.create') }}" class="btn btn-primary mb-3">
        <strong>+</strong> New note
    </a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Created at</th>
                <th scope="col">Updated at</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($notes as $note)
                <tr>
                    <td>
                        <a href="{{ route('notes.show', $note) }}">
                            {{ $note->title }}
                        </a>
                    </td>
                    <td>{{ $note->body }}</td>
                    <td>{{ $note->created_at }}</td>
                    <td>{{ $note->updated_at }}</td>
                    <td>
                        <form action="{{ route('notes.destroy' , $note)}}" method="POST">
                            @method('delete')
                            {!! csrf_field()!!}
                            <button type="submit" class="btn btn-sm btn-danger">
                                <strong>x</strong> Verwijderen
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
