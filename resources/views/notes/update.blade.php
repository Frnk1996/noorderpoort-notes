@extends('layout.base')
@section('title', 'Update note')
@section('content')
    <form action="{{ route('notes.update', $note) }}" method="post">
        @method('PUT')
        {!! csrf_field()!!}
        <div class="form-group">
            <label class="control-label" for="title">Title</label>
            <input type="text"
                   value="{{ $note->title }}"
                   required=""
                   name="title"
                   id="title"
                   class="form-control"
            >
        </div>

        <div class="form-group">
            <label class="control-label" for="body">Body</label>
            <textarea name="body" id="body" class="form-control" rows="10">{{ $note->body }}</textarea>
        </div>
        <button class="btn btn-success btn-block">Submit note</button>
    </form>
@endsection
