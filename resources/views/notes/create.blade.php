@extends('layout.base')
@section('title', 'New note')
@section('content')
    <form action="{{ route('notes.store') }}" method="post">
        {!! csrf_field()!!}
        <div class="form-group">
            <label class="control-label" for="title">Title</label>
            <input type="text"
                   required=""
                   name="title"
                   id="title"
                   class="form-control"
            >
        </div>

        <div class="form-group">
            <label class="control-label" for="body">Body</label>
            <textarea
                   name="body"
                   id="body"
                   class="form-control"
                   rows="10"
            ></textarea>
        </div>
        <button class="btn btn-success btn-block">Submit note</button>
    </form>
@endsection
