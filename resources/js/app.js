require('./bootstrap');

/**
 * Import Dependencies
 */
import { createApp } from 'vue'
import VueAxios from 'vue-axios'

/**
 * Init Vue app
 */
const app = createApp({})

/**
 * Use dependencies
 */
app.use(VueAxios, axios)

/**
 * Register components
 */
app.component('Notes', require('./components/Notes.vue').default);

/**
 * Mount app
 */
app.mount('#vue-root')
